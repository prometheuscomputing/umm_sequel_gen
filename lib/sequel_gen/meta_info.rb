# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module UmmSequelModels
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # Required Strings  
  GEM_NAME = "umm_sequel_gen"
  VERSION  = '0.1.2'
  SUMMARY  = %q{Generates Ruby Sequel Models from a UML model expressed as instances of the metamodel defined in the uml_metamodel gem.}
  
  # Optional Strings
  DESCRIPTION = %q{Generates Ruby Sequel Models from a UML model expressed as instances of the metamodel defined in the uml_metamodel gem.}
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/umm_sequel_gen'
  
  # Optional Strings or Arrays of Strings
  AUTHORS = ["Michael Faughn"]
  EMAILS  = ["michael.faughn@nist.gov"]
  
  LANGUAGE = :ruby # This specifies the language the project is written
  
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['>= 2.7']
  
  RUNTIME_VERSIONS = {
    :mri   => ['>= 2.7']
  }
  
  APP_TYPES = [] # This is a library, not an app.
  LAUNCHER = nil # This is a library, not an app.

  DEPENDENCIES_RUBY  = {
    :umm_driven_builder => '~> 0.0',
    :Foundation         => '~> 1.8',
    :rainbow            => '~> 3.0'
  }
  
end
