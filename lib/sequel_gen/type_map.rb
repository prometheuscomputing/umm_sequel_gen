class SequelBuilder < UMMDrivenBuilder
  def type_map
    self.class.type_map
  end
  
  def self.type_map
    @type_map ||= {
      "UML::String"     => "String",
      "UML::Integer"    => "Integer",
      "UML::Boolean"    => "TrueClass",
      "UML::Null"       => "Nil",
      "UML::Real"       => "Float",
      "UML::ByteString" => "::File",
      "UML::Char"       => "String",
      "UML::Date"       => "Time",
      "UML::Float"      => "Float",
      "UML::Datetime"   => "Time",
      "UML::Time"       => "Time",
      "UML::UnlimitedNatural"  => "Integer",
      "UML::RegularExpression" => "String",
      "Gui_Builder_Profile::Time" => "Time",
      "Gui_Builder_Profile::Date" => "Time",
      "Gui_Builder_Profile::Timestamp" => "Time"
    }
    
    # # FIXME
    # if defined?(XML)
    #   xsd = {
    #     XML::AnyURI       => String,
    #     XML::Decimal      => Float,
    #     XML::Duration     => String,
    #     XML::GYear        => String,
    #     XML::GYearMonth   => String,
    #     XML::Language     => String,
    #     XML::Notation     => String,
    #     XML::QName        => String,
    #     XML::Token        => String,
    #     XML::UnsignedInt  => Integer,
    #     XML::UnsignedLong => Integer,
    #     XML::NormalizedString   => String,
    #     XML::NonNegativeInteger => Integer
    #   }
    #   map = map.merge(xsd)
    # end
    # @type_map
  end
end