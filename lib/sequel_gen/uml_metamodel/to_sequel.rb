require 'rainbow'
module UmlMetamodel
  class ElementBase
    attr_accessor :adaptation
    attr_accessor :adaptation_elements # those elements created in order to facilitate the creation of necessary Sequel classes
    def prepare_sequel_gen(builder)
      # puts "Preparing #{to_dsl}"
      @adaptation_elements = []
      e = errors_and_warnings()[:errors]
      STDERR.puts "Invalid Element: #{to_dsl}" if e && !e.empty?
      # raise "Invalid Element: #{to_dsl}" if e && !e.empty?
      builder.register(self)
    end
    private
    def read_template(name)
      File.read(File.join(SequelBuilder::TEMPLATE_DIR, name))
    end
    def documentation_string
      documentation && !documentation.empty? ? ":documentation => #{documentation.inspect}" : nil
    end
  end
  
  class Element    
    def end_declaration(builder)
      builder.unindent
      builder.sequel("end # #{self.class.name.demodulize} '#{qualified_name}'")
    end
  end
  
  class Project
    def prepare_sequel_gen(builder)
      contents.each { |e| e.prepare_sequel_gen(builder) }
    end
    def sequelgen_contents
      contents.sort_by { |c| [c.class.to_s, c.name.to_s] } + 
      adaptation_elements.sort_by { |c| [c.class.to_s, c.name.to_s] }
    end
  end
  
  class Package
    
    def prepare_sequel_gen(builder)
      super
      contents.each { |e| e.prepare_sequel_gen(builder) }
    end
    
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      return if contents.empty? # because this package will serve no purpose if it is empty
      builder.sequel(builder.skip) if builder.indentation < 1
      builder.sequel("module #{name}")
      builder.indent
      builder.sequel("@documentation = #{documentation.inspect}") if documentation && !documentation.empty?
      packages.each { |pkg| pkg.declare(builder) }
      end_declaration(builder)
    end
    
    def sequelgen_contents
      sort_elements(contents) + sort_elements(adaptation_elements)
    end
  end
  
  class Classifier
    def prepare_sequel_gen(builder)
      super
      contents.each { |e| e.prepare_sequel_gen(builder) } # Not sure what this will actually get.  Inner classes?
      properties.each { |e| e.prepare_sequel_gen(builder) } unless primitive? # while primitives may have properties that take part in associations, they do not get implemented
    end
    
    def class_declaration_and_parents(builder)
      builder.sequel(builder.skip)
      builder.sequel("# ============= Begin #{self.class.name.demodulize} '#{name}' =============")
      class_line = "class #{qualified_name}"
      child_of_lines = []
      # This is a HACK to handle the fact that Gui_Builder_Profile does not have some of its primitives inherit from a base primitive like it should.
      if package.name == "Gui_Builder_Profile" && primitive?
        if name =~ /^(Date|Time|Timestamp)$/
          t = UmlMetamodel::PRIMITIVES[:time]
          add_parent(t) unless parents.include?(t)
        end
      end
      if parents.any?
        first_parent = parents.first
        first_parent_name = first_parent.package.qualified_name == 'UML' ? first_parent.name : first_parent.qualified_name
        # And here is a HACK to handle subclasses of Boolean, which you really shouldn't have anyway.
        first_parent_name = 'TrueClass' if first_parent_name == 'Boolean'
        class_line << " < #{first_parent_name}"
        parents[1..-1].each do |parent|
          child_of_lines << "child_of #{parent.qualified_name}"
        end
      else
        class_line << " < Sequel::Model"# unless primitive?
      end
      builder.sequel(class_line)
      builder.indent
      builder.sequel("@documentation = #{documentation.inspect}") if documentation && !documentation.empty?
      builder.sequel(child_of_lines) if child_of_lines.any?
    end
    
    def dataset_and_plugins(builder)
      builder.sequel("set_dataset :#{Appellation.table_name(self)}")
      if SequelBuilder::PLUGINS.any?
        plugin_string = SequelBuilder::PLUGINS.collect { |pl| "plugin :#{pl}" }.join("; ")
        builder.sequel(plugin_string)
      end
    end
    
    def declare_properties(builder, opts = {})
      opts = {:ignore => []}.merge(opts)
      # put everything that is realized as an attribute at the top
      sorted = properties.sort_by { |prop| (prop.type.primitive? && prop.upper == 1) ? 1 : 0 }
      sorted.each { |prop| prop.declare(builder) unless opts[:ignore].include?(prop.name) }
      adaptation_elements.each { |e| remove_property(e) if e.adaptation == :primitive_wrapper_duplicate }
      # properties.each { |prop| prop.declare(builder) unless opts[:ignore].include?(prop.name) }
    end
    
    def sequelgen_properties
      sort_properties(properties) + sort_properties(adaptation_elements)
    end
  end

  class Class
    attr_accessor :join_class_for
    
    def add_interfaces(builder)
      implements.each do |interface|
        builder.sequel("implements #{interface.qualified_name}" )
      end
    end
    
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      builder.schema("#{qualified_name}.create_schema?")
      class_declaration_and_parents(builder)
      dataset_and_plugins(builder)
      add_interfaces(builder)
      builder.sequel("association_class!") if association_class_for
      builder.sequel("abstract!") if is_abstract
      builder.sequel("complex_attribute!") if complex_attribute?
      builder.sequel("singleton!") if is_singleton
      associates = association_class_for || join_class_for
      if associates
        associates.properties.collect do |prop|
          getter = Appellation.singular_role(prop)
          code = "associates :'#{prop.type.qualified_name}' => :#{getter}"
          code << ", :opp_is_ordered => true" if prop.opposite.is_ordered
          code
          builder.sequel(code)
        end
      end
      declare_properties(builder)
      end_declaration(builder)
    end
  end
  
  class Interface
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      class_declaration_and_parents(builder)
      dataset_and_plugins(builder)
      builder.sequel("interface!")
      declare_properties(builder)
      end_declaration(builder)
    end
  end
  
  class Primitive
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      class_declaration_and_parents(builder)
      builder.sequel("extend ModelMetadata")
      end_declaration(builder)
    end
  end
  
  class Datatype
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      builder.schema("#{qualified_name}.create_schema?")
      class_declaration_and_parents(builder)
      dataset_and_plugins(builder)
      declare_properties(builder)
      end_declaration(builder)
    end
  end
  
  class Enumeration
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      builder.schema("#{qualified_name}.create_schema?")
      class_declaration_and_parents(builder)
      dataset_and_plugins(builder)
      builder.sequel("enumeration!")
      declare_properties(builder)
      end_declaration(builder)
    end
  end
  
  class Property
    attr_accessor :modeled_as
    
    def self.validate_property_owner_proc
      Proc.new do |prop|
        valid_adaptations = [:primitive_wrapper_duplicate] 
        prop.adaptation && valid_adaptations.include?(prop.adaptation)
      end
    end
    
    # I do not think we cover the case where an association is modeled directly to a base primitive (e.g. UML::String)
    def prepare_sequel_gen(builder)
      # return unless type # TODO do we want to ignore properties without types? Or raise?  I think we want to raise....
      # important that this is only called once! and at the beginning, before additional, related elements have been created
      @modeled_as ||= association ? :association : :attribute
      # If it is part of an association then preparation is handeled by the association
      if association
        prepare_multiprimitive(builder) if type.primitive? && upper > 1
      elsif type.enumeration?
        prepare_enumeration_attribute(builder)
      elsif type.primitive?
        prepare_multiprimitive(builder) if upper > 1
      elsif type.complex_attribute?
        if upper > 1
          raise "Complex Attributes with multiplicity greater than one are not implemented! #{self.inspect}"
        end
      else # then it is an attribute typed as a non-primitive
        prepare_association_for_attribute(builder)
      end
    end
    
    def declare(builder)
      return if builder.declared?(self)
      builder.declare_element(self)
      if association
        if type.primitive? 
          return if upper > 1 # there is a proxy / primitive wrapper that will be declared in place of this property
          declare_primitive_type_attribute(builder)
        else
          declare_association(builder)
        end
      else
        declare_primitive_type_attribute(builder)
      end
    end
    
    def declare_primitive_type_attribute(builder)
      return if upper > 1
      type_name = builder.type_map[type.qualified_name] || type.qualified_name
      builder.sequel("attribute :#{Appellation.role(self)}, #{type_name}, :display_name => '#{Appellation.label(self)}', :additional_data => { #{additional_data} }")
    end
    
    def prepare_multiprimitive(builder)
      # We need to create a class that will drive the creation of a table to hold instances of the primitive that this property is typed as.  We will call this the "wrapper class"
      wc = UmlMetamodel::Class.new(Appellation.primitive_wrapper_class_name(self))
      wc.documentation = "This class was created in order to facilitate the implementation of an attribute typed as a primitive with a multiplicity of greater than one in the persistance model (e.g. within the context of an ORM)"
      wc.owner = owner.package # this is scary because it happens while we are iterating over owner.package.classifiers!
      owner.package.adaptation_elements << wc
      wc.adaptation = :primitive_wrapper_class
       
      # The wrapper class needs a property that will hold the value of one instance of the primitive that this property is typed as
      wrapped_property = UmlMetamodel::Property.new(Appellation.singular_role(self)) # should singularize name
      wrapped_property.lower      = 0
      wrapped_property.upper      = 1
      wrapped_property.is_ordered = true # may not strictly be the case but UML sort of implies that this is an array rather than a bag, if nothing else then by its notation using square brackets.
      wrapped_property.is_unique   = is_unique
      wrapped_property.is_derived  = is_derived
      wrapped_property.aggregation = aggregation
      wrapped_property.type        = type # FIXME this is adding the wrapped property to the collection of properties for type.properties_of_type.
      wc.add_property(wrapped_property)
      
      # create the property from the wrapper class we just created back to the class that owns this property
      reciprocal_property = UmlMetamodel::Property.new(Appellation.reciprocal_property_name_for_primitive_wrapper_class(self))
      reciprocal_property.lower = 1
      reciprocal_property.upper = 1
      reciprocal_property.type  = owner
      wc.add_property(reciprocal_property)
      
      # And create an association b/w the owner of this class and the wrapper class
      # First duplicate the original property (self).  This will be a property of self.owner but since owner is a Primitive, it won't actually get implemented in the application domain (i.e. primitives are not Sequel::Models, won't have any methods associated with the property, and of course there will be no table for the primitive class)
      stand_in_property = UmlMetamodel::Property.new(Appellation.role(self))
      owner.add_property(stand_in_property)
      owner.adaptation_elements << stand_in_property
      stand_in_property.adaptation = :primitive_wrapper_duplicate
      stand_in_property.upper      = Float::INFINITY
      stand_in_property.modeled_as = modeled_as

      
      # NOTE: creating a new association and passing it some properties ends up setting the type on the properites, which is correct from the UML standpoint but is a potential problem here because we are creating a wrapper class (think of it as similar to the autoboxing of Primitives in Java).  Just be aware of this if you are seeing strange things that are causing problems.
      association           = UmlMetamodel::Association.new(:properties => [stand_in_property, reciprocal_property])
      association.name      = Appellation.association_name(association)
      association.wraps     = wrapped_property.type
      association.primitive = wrapped_property.name
      owner.package.adaptation_elements << association # not strictly necessary because it isn't a many-to-many but we'll do it anyway
      association.adaptation = :primitive_wrapper_association
    end
    
    def prepare_association_for_attribute(builder, opts = {})
      multiplicity = opts[:multiplicity]
      force_many   = opts[:force_many]
      assoc = UmlMetamodel::Association.new
      assoc.package = owner.package
      owner.package.adaptation_elements << assoc
      assoc.adaptation = opts[:enumeration] ? :association_for_enumeration_attribute : :association_for_attribute
      opp = UmlMetamodel::Property.new(nil)
      opp.type  = owner
      opp.owner = type
      self.association = assoc
      opp.association  = assoc
      assoc.properties = [self, opp]
      # FIXME this should be taken care of inside Appellation if possible (it might not be super simple to do...)
      opp.name  = (Appellation.role(owner, :multiplicity => multiplicity)) + "_with_" + name
      opp.upper = Float::INFINITY
      assoc.prepare_sequel_gen(builder, :force_many => force_many) if multiplicity == :plural || force_many == true
    end
    
    def prepare_enumeration_attribute(builder)
      prepare_association_for_attribute(builder, :multiplicity => :plural, :force_many => true, :enumeration => true)
    end
    
    def declare_association(builder)
      getter       = Appellation.role(self)
      opp          = opposite
      opp_getter   = Appellation.role(opp)
      s_opp_getter = Appellation.singular_role(opp)
      if adaptation == :primitive_wrapper_duplicate
        s_opp_getter = opp_getter
      end
 
      association_data = ":'#{type.qualified_name}', :getter => :#{getter}, :opp_getter => :#{opp_getter}, :singular_opp_getter => :#{s_opp_getter}, :display_name => '#{Appellation.label(self)}'"
      association_data << ", :wraps => :'#{association.wraps.qualified_name}', :primitive => :#{association.primitive}" if association.wraps
      association_data << ", :enumeration => true" if type.is_a?(UmlMetamodel::Enumeration)
      association_data << ", :composition => true" if composite?
      association_data << ", :shared => true" if shared?
      association_data << ", :ordered => true" if is_ordered
      association_data << ", :opp_is_ordered => true" if opposite.is_ordered
      if upper > 1
        if opp.upper > 1 # many to many
          association_data << ", :through => :'#{association.join_class.qualified_name}'"
          type = "many_to_many"
        else # one_to_many
          type = "one_to_many"
          association_data << ", :through => :'#{association.association_class.qualified_name}'" if association.association_class
        end
      else
        if opp.upper > 1 # many to one
          type = "many_to_one"
        else # one to one
          # This holds_key business being backwards compatible is dependent on modelGen having ordered the properties in the Association the same way that the old plugin.sequelGen did...and THAT is a little bit scary.
          holds_key = composite? || shared? || (!(opposite.composite? || opposite.shared?) && association.properties.first == self)
          association_data << ", :holds_key => true" if holds_key && !association.association_class
          type = "one_to_one"
        end
        association_data << ", :through => :'#{association.association_class.qualified_name}'" if association.association_class
      end
      association_data << ", :additional_data => { #{additional_data}}"
      builder.sequel("#{type} #{association_data}")
    end
    
    private
    def additional_data
      data = []
      data << documentation_string.to_s if documentation_string
      data << ":modeled_as => #{modeled_as == :association ? ':association' : ':attribute'}"
      data << ":lower => #{lower}, :upper => #{upper}"
      data.join(", ")
    end
  end
  
  class Association
    attr_reader :join_class
    attr_accessor :wraps, :primitive
    
    def prepare_sequel_gen(builder, opts = {})
      super(builder)
      # puts Rainbow(inspect).yellow
      # properties.each{|x| puts x.to_dsl}
      make_join_class(builder) if !(association_class || join_class) && (opts[:force_many] || properties.all? { |prop| prop.upper > 1 })
    end
    
    # def make_join_class?
    #   return false if association_class # already got made because by self.association_class
    #   return true if properties.all? { |prop| prop.upper > 1 }
    #   false
    # end
    
    def join_class_name
      @join_class_name ||= Appellation.association_name(self)
    end
    
    def join_class_qualified_name
      owner.qualified_name + '::' + join_class_name
    end
    
    def join_class
      @join_class || association_class
    end
    
    def make_join_class(builder)
      @join_class = UmlMetamodel::Class.new(join_class_name)
      @join_class.package = package
      @join_class.join_class_for = self
      owner.adaptation_elements << @join_class
      @join_class.adaptation = :join_class
      do_backwards_compatibility(builder)
      @join_class
    end
    
    # This was grabbed from the old sequelGen and reworked to work for instances of UmlMetamodel::Association.  Use of this alternative name is intended to support backward compatibility.
    # We can't know with 100% surety what the order of the attributes would be with the old version of the generator so we need to be able to cover both possible cases
    def legacy_name(reverse = false, use_role = false)
      if name.to_s.empty?
        end_one = properties.first
        end_two = properties.last
        ordered_attrs = [end_one, end_two].sort_by { |attribute| attribute.owner.name.gsub("-", "_")}
        ordered_attrs.reverse! if reverse
        attr1, attr2 = ordered_attrs
        role = 'Association'
        role = attr1.name || attr2.name || 'Association' if use_role
        "#{attr1.owner.name.gsub("-", "_")}_#{attr2.owner.name.gsub("-", "_")}_#{role.gsub("-", "_").camelize}"
      else
        name.to_s.gsub("-", "_")
      end
    end
    
    def legacy_table_name(reverse = false, use_role = false)
      Appellation.table_name(legacy_name(reverse, use_role))
    end
    
    def legacy_qualified_name(reverse = false, use_role = false)
      package.qualified_name + "::#{legacy_name(reverse, use_role)}"
    end
    private :legacy_qualified_name
      
    # def declare(builder)
    #   return if builder.declared?(self)
    #   builder.declare_element(self)
    #   # TODO the real issue is going to be with dataset names...is there some code that could be injected that would change dataset names on existing datasets?
    #   # TODO write out newly created UmlMetamodel elements to file that will get passed along to output
    #    if join_class
    #     join_class.declare(builder)
    #     do_backwards_compatibility(builder)
    #   end
    # end
    
    def do_backwards_compatibility(builder)
      [true, false].each do |reverse|
        [true, false].each do |use_role|
          builder.legacy("if DB.tables.include?(:#{legacy_table_name(reverse, use_role)})")
          builder.legacy("  #{legacy_qualified_name(reverse, use_role)} = #{join_class_qualified_name}")
          builder.legacy("  DB.rename_table(:#{legacy_table_name(reverse, use_role)}, #{join_class_qualified_name}.table_name)")
          builder.legacy("end")
        end
      end
    end
  end
  
  class EnumerationLiteral
    def to_instance_spec
      "#{enumeration.qualified_name}.where(:value => " + name.to_s.inspect + ").first || #{enumeration.qualified_name}.create(:value => " + name.to_s.inspect + ")"
    end
  end
end
