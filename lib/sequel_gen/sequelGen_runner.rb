require 'tempfile'
require 'magicdraw_plugin_runner/plugin_runner'

# Load magicdraw_extensions
# Only magic_draw_stereotypes is used by this file, but it seems to mess up the "load 'magicdraw_extensions.rb'" call later,
# so other magicdraw_extensions are required here as well
# require 'magicdraw_extensions' # <- Seems to be broken.  Well, yes it is.  BUT WHY????
require 'magicdraw_extensions/object_extensions'
require 'magicdraw_extensions/magic_draw_stereotypes'
require 'magicdraw_extensions/magic_draw_nav'
require 'magicdraw_extensions/project'

class SequelGenRunner < MagicdrawPluginRunner::PluginRunner
  def setup_for_project_load
    # Example of how to setup default project path or arguments
    #self.project_path ||= relative('test.mdzip')
    #self.args[0] ||= 'Test'
  end
  
  def activate_plugin(md_env = {})
    app     = md_env[:app]
    project = md_env[:project]
    package = args[0]
    
    # Get the specified package
    model      = MagicDraw.root_model
    packages   = [model] + model.getNestedPackage 
    md_package = packages.find { |np| np.getName == package }
    raise "Couldn't find package #{package}!" unless md_package
    
    # Set any forced options
    generation_package = nil; output_dsl_file = nil; import_test_input = nil
    # args.each do |arg|
    #   input_dsl_arg     = /--input_dsl_file=(.+)/.match(arg)
    #   import_test_input = input_dsl_arg[1] if input_dsl_arg
    #   output_dsl_arg    = /--output_dsl_file=(.+)/.match(arg)
    #   output_dsl_file   = output_dsl_arg[1] if output_dsl_arg
    #   pkg_arg  = /--generation_package=(.+)/.match(arg)
    #   generation_package = pkg_arg[1] if pkg_arg
    # end
    # Select md_package in containment tree
    browser          = app.getMainFrame.getBrowser
    containment_tree = browser.getContainmentTree
    containment_tree.openNode(md_package, select=true, appendSelection=false, requestFocus=true, scrollToVisible=false)
    
    # How to navigate and use nodes:
    # project_node    = containment_tree.getRootNode
    # data_node       = project_node.children.first
    # md_package_node = data_node.children.find { |c| c.getUserObject.getName == package }
    
    # Trigger the plugin action
    # actions_menu_creator = com.nomagic.magicdraw.actions.MenuCreatorFactory.getMenuCreator
    ap               = com.nomagic.magicdraw.actions.ActionsProvider.getInstance
    actions_manager  = ap.getContainmentBrowserContextActions(containment_tree)
    actions          = actions_manager.getAllActions
    sequelGen_action = "Generate Ruby Sequel Code"
    import_action    = actions.find { |a| puts "Found #{a.getName}"; a.getName == sequelGen_action }
    raise "Could not find context action '#{sequelGen_action}'!" unless import_action
    import_action.actionPerformed(nil)
    
  end
end

$plugin_runner = SequelGenRunner