require 'sequel_gen/requirements'
require_relative 'type_map'
load 'sequel_gen/uml_metamodel/to_sequel.rb'

class SequelBuilder < UMMDrivenBuilder
  
  # attr_reader :model
  # attr_reader :metainfo
  TEMPLATE_DIR = File.join(File.expand_path(File.dirname(__FILE__)), "templates")
  PLUGINS = [:specific_associations, :timestamps, :boolean_readers]
  
  def self.generate_ruby_sequel_from_umm_project(project)
    new(project).build
  end
    
  def initialize(project)
    super
    @sequel            = []
    @schema            = []
    @enum_literals     = []
    @legacy_code       = []
    @declared_elements = []
    @indentation       = 0
    @change_tracked    = UmlMetamodel.find("Gui_Builder_Profile::change_tracked", @project)
    PLUGINS << :change_tracker if @change_tracked
  end

  def build
    @project.prepare_sequel_gen(self)
    # puts @registry.pretty_inspect"
    order_packages
    order_classifiers
    @ordered_elements.each { |e| e.declare(self) }
    generated_code = {}
    generated_code[:sequel] = @sequel.each(&:rstrip!).join("\n")
    generated_code[:schema] = @schema.each(&:rstrip!).join("\n")
    generated_code[:enum_literals] = enumeration_literal_instances.each(&:rstrip!).join("\n")
    generated_code[:legacy_code] = @legacy_code.each(&:rstrip!).join("\n")
    write_output(generated_code)
    "Success"
  end
  
  def declare_element(element)
    @declared_elements << element
  end

  def declared?(element)
    @declared_elements.include?(element)
  end

  def sequel(strings)
    _write(@sequel, strings)
  end

  def schema(strings)
    _write(@schema, strings)
  end
  
  def legacy(strings)
    _write(@legacy_code, strings)
  end

  def sequel_and_schema(strings)
    schema(strings)
    sequel(strings)
  end
  
  def enumeration_literal_instances
    enumeration_instances_code = ["seqs = DB[:sqlite_sequence]", "ChangeTracker.start if defined?(ChangeTracker) && !ChangeTracker.started?"]
    enums = @project.packages(:include_imports => true, :recurse => true).collect { |pkg| pkg.enumerations }.flatten    
    enums.each do |enum|
      enumeration_instances_code << "if seqs.empty? || seqs.where(:name => #{enum.qualified_name}.table_name.to_s).first.nil? || seqs.where(:name => #{enum.qualified_name}.table_name.to_s).first[:seq] < 1"
      enum.literals.each do |literal|
        enumeration_instances_code << "  #{literal.to_instance_spec}"
      end
      enumeration_instances_code << "end"
    end
    enumeration_instances_code << "ChangeTracker.commit if defined?(ChangeTracker)"
    enumeration_instances_code
  end
  
  def write_output(generated_code)
    sequel_code = prepend_sequel_code(generated_code[:sequel]) + add_legacy_code(generated_code[:legacy_code])
    
    sql_generation_filename = 'generate_sql.rb'
    sql_generation_code     = []
    schema_filename         = 'schema.rb'
    schema_code             = []
    driver_filename         = 'driver.rb'
    driver_code             = []
    modifications_code      = []
    modifications_filename  = 'sequel_model_modifications.rb'
    models_filename         = 'models.rb'
    uml_filename            = 'uml.rb'

    top_module_constants = @project.packages.reject { |pkg| pkg.is_a?(UmlMetamodel::Model) }.collect { |pkg| "'#{pkg.qualified_name}'" }.sort_by { |str| str =~ /Gui_Builder_Profile|GuiBuilderProfile/ ? 0 : 1 } # is this sufficient in all cases? FIXME add XSD?
    
    project_main_template = read_template('project_main.rb.erb')
    project_main_code     = ERB.new(project_main_template, nil, "%<>").result(binding)
    PrometheusFiles.save_to_file("#{generated_gem_name}.rb", project_main_code, [generated_gem_name, 'lib'])
    # Construct meta_info.rb file
    meta_info_template  = read_template('meta_info.rb.erb')
    meta_info_code      = ERB.new(meta_info_template, nil, "%<>").result(binding)
    PrometheusFiles.save_to_file("meta_info.rb", meta_info_code, [generated_gem_name, 'lib', generated_gem_name])
    
    # Set up the sequel generation code
    sql_generation_code << read_template('require_gems.rb')
    sql_generation_code << read_template('sql_generation.rb')
    
    # Set up the ruby driver
    driver_template = read_template('ruby_driver.rb.erb')
    driver_code << ERB.new(driver_template, nil, "%<>").result(binding)

    modifications_code << read_template('sequel_model_modifications.rb')

    enumeration_instances_filename = 'enumeration_instances.rb'

    # Save code to files
    model_dir_array = [generated_gem_name, 'lib', generated_gem_name, 'model']
    PrometheusFiles.save_to_file(sql_generation_filename, sql_generation_code.join("\n"), model_dir_array)
    PrometheusFiles.save_to_file(schema_filename, generated_code[:schema], model_dir_array)
    PrometheusFiles.save_to_file(models_filename, sequel_code, model_dir_array)
    PrometheusFiles.save_to_file(modifications_filename, modifications_code.join("\n"), model_dir_array)
    # TODO: remove duplicate line below. As of 6/26/2010, the GUI builder is still dependent on it
    PrometheusFiles.save_to_file(driver_filename, driver_code.join("\n"), model_dir_array)
    PrometheusFiles.save_to_file(enumeration_instances_filename, generated_code[:enum_literals], model_dir_array)
    PrometheusFiles.save_to_file(uml_filename, @project.to_dsl(:unsorted => true, :custom_content => :sequelgen_content, :custom_properties => :sequelgen_properties, :validate_property_owner_proc => UmlMetamodel::Property.validate_property_owner_proc), model_dir_array)
  end
  
  def prepend_sequel_code(sc)
    pre = "require_relative 'sequel_model_modifications'\n"
    pre << "Infinity = Float::INFINITY unless defined?(Infinity)\n\n"
    pre << "#------------- Start Module code from UML ------------------\n"
    pre + sc
  end
  
  def add_legacy_code(code)
    return code if code.empty?
    lc =  "\n\n#---------------------------------------------------\n"
    lc << "#--- Included to support Backwards compatibility ---\n"
    lc << "#---------------------------------------------------\n"
    lc << code
    lc
  end

  def read_template(name)
    File.read(File.join(TEMPLATE_DIR, name))
  end
end