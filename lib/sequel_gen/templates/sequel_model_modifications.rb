module SequelDateTimePatch
  # typecast_value_datetime does not like a java.util.Date object, so we need
  # to convert it before we get there.  DB should extend this module so that it
  # goes here before it reaches Datebase.typecast_value_datetime
  def typecast_value_datetime(value)
    if value.kind_of?(java::util::Date)
      super DateTime.parse(value.to_s)
    else
      super
    end
  end
end

DB.extend(SequelDateTimePatch) if defined?(Java)

module SequelDatasetPatch
  # Split out from fetch rows to allow processing of JDBC result sets
  # that don't come from issuing an SQL string.
  def process_result_set(result)
    # get column names
    meta = result.getMetaData
    cols = []
    i = 0
    meta.getColumnCount.times{cols << [output_identifier(meta.getColumnLabel(i+=1)), i]}
    @columns = cols.map{|c| c.at(0)}
    row = {}
    blk = if @convert_types
      lambda{|n, i| 
        table = meta.getTableName i
        unless table.nil? || table.to_s.empty? || table.to_s == "sqlite_master"
          col = n
          # puts DB.schema(table).inspect
          column_info = DB.schema(table).select {|c| c[0] == col}.first[1]
          column_type = column_info[:type]
        end
      
        if defined?(column_type) && column_type == :datetime
          obj = result.getObject(i)
          row[n] = obj.nil? ? nil : Time.parse(obj)
        else
          row[n] = convert_type(result.getObject(i))
        end
      }
    else
      lambda{|n, i| row[n] = result.getObject(i)}
    end
    # get rows
    while result.next
      row = {}
      cols.each(&blk)
      yield row
    end
  end
end

if RUBY_PLATFORM =~ /java/
  unless defined?(Sequel::JDBC) && defined?(Sequel::JDBC::SQLite)
    require 'sequel/adapters/jdbc'
    require 'sequel/adapters/jdbc/sqlite'
  end

  unless defined?(Sequel::JDBC::SQLite::Dataset)
    module Sequel::JDBC::SQLite::Dataset
    end
  end

  Sequel::JDBC::SQLite::Dataset.send(:include, SequelDatasetPatch)
end

module SequelModifications
  module InstanceMethods
    if defined?(Java)
      def after_initialize
        super
        fix_java_datetimes
      end # End after_initialize hook
      
      def refresh
        return_val = super
        fix_java_datetimes
      end
      
      private
      def fix_java_datetimes
        db_schema.each do |column, info_hash|
          next unless info_hash[:type] == :datetime
          val = self[column]
          if val && ([String, Java::java.lang.String].any?{|k| val.kind_of?(k)})
            format = Java::java.text.SimpleDateFormat.new("yyyy-MM-dd HH:mm:ss.SSSSSS")
            new_value = format.parse(val)
            self[column] = new_value if new_value
          end
        end
      end
    end # End if defined?(Java)
  end # End InstanceMethods module
end

class Time
  # Extend the comparision operator so we can compare Java Date objects to
  # Ruby Time objects
  alias_method :orig_compare, :<=> unless public_method_defined?(:orig_compare)
  def <=> other
    defined?(Java) && other.kind_of?(java.util.Date) && \
      other = Time.at(other.time/1000)
    orig_compare other
  end
end

class Sequel::Model
  plugin SequelModifications
end
