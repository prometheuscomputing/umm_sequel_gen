Kernel.warn "Use of generate_sql.rb is untested and is deprecated!"
require 'logger'
DB = Sequel.sqlite # Use in-memory DB
class SQLLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{msg}\n" if severity.to_s == 'INFO'
  end
end

model_dir = File.expand_path(File.dirname(__FILE__))
SQL_FILE = File.join(model_dir, 'SQL.sql')

File.delete(SQL_FILE) if File.exists?(SQL_FILE) && ! File.directory?(SQL_FILE)
DB.loggers << SQLLogger.new(SQL_FILE)

require File.join(model_dir, 'schema.rb')
