# Setup bundler for both jruby and ruby. 
# TODO: This should be split in schemaGen into different files
# _________________ Begin Bundler setup _________________
model_dir = File.dirname(__FILE__)
# ref_impl_dir = File.expand_path(File.join(ENV['HOME'], '.ref_impl'))
# bundler_jar  = File.join(ref_impl_dir, 'bundler.jar')
# gemfile = File.join(model_dir, 'Gemfile')

using_jruby = (RUBY_PLATFORM =~ /java/)

require 'rubygems'
require 'sequel'

require 'sequel_specific_associations'
# Require Sequel's optional inflector
require 'sequel/extensions/inflector'
