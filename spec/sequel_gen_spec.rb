require 'spec_helper'
require 'fileutils'
require_relative '../lib/sequel_gen/sequel_builder'
# require 'nokogiri'
# require 'Foundation/load_path_management'
# FileUtils.remove_dir($sequel_gen_test_dir) if File.exist?($sequel_gen_test_dir)

describe 'sequel_gen' do
  before :all do
    @test_data_dir = File.expand_path('../../test_data', __FILE__)
    expect(File.exist?(@test_data_dir)).to be_truthy
  end
  
  # it "should accept a UmlMetamodel::Project and produce SSA code from it." do
  #   expect()
  # end
  
  it "should correctly create associations for primitive properties with a multiplicity > 1" do
    dsl_file = File.join(@test_data_dir, 'MultiPrimitivesTest.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
    expect(message).not_to include('Error')
  end
  
  it "should correctly create associations for properties typed as enumerations" do
    dsl_file = File.join(@test_data_dir, 'EnumTest.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
    expect(message).not_to include('Error')
  end
  
  it "should correctly create associations for attributes typed as primitives with multiplicity greater than 1" do
    dsl_file = File.join(@test_data_dir, 'MultiPrimitivesTest.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
    expect(message).not_to include('Error')
  end

  it "should not produce any errors" do
    dsl_file = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
    expect(message).not_to include('Error')
  end
  
  it "should not produce any errors when dealing with subclasses of Boolean" do
    dsl_file = File.join(@test_data_dir, 'BooleanSubclassTest.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SequelBuilder.generate_ruby_sequel_from_umm_project(umm)
    expect(message).not_to include('Error')
  end

  # Test with different options
  # it "should work with the use_collection_containers option" do
  #   test_collection_schema_filename = 'TestCollections.xsd'
  #   test_collection_schema = File.join(@code_dir, test_collection_schema_filename)
  #   FileUtils.rm(test_collection_schema) if File.exist?(test_collection_schema)
  #   @message = MagicdrawPluginRunner.run(relative('../lib/sequel_gen/sequel_gen_runner.rb'), [@car_example_model, 'Application',
  #     "--output_dirname=sequel_gen_test2"])
  #   File.exist?(test_collection_schema).should be_true
  #   File.read(test_collection_schema).should include('VoteCountsCollection')
  #
  # end
end