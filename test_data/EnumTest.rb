project "EnumTest" do
  package "Automotive" do
    klass "Car", :parents => ["Automotive::Vehicle"]
    klass "Vehicle", :is_abstract => true do
      property "make", :type => "Automotive::VehicleMaker", :default_value => "Unknown"
    end
    enumeration "DomesticVehicleMaker", :parents => ["Automotive::VehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Dodge"
      literal "Ford"
    end
    enumeration "ForeignVehicleMaker", :parents => ["Automotive::VehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Honda"
      literal "Volvo"
    end
    enumeration "OtherVehicleMaker", :parents => ["Automotive::DomesticVehicleMaker", "Automotive::ForeignVehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Home Made"
      literal "Other"
      literal "Unknown"
    end
    enumeration "VehicleMaker" do
      property "value", :type => "UML::String"
    end
  end
  package "People" do
    klass "Person" do
      property "handedness", :type => "People::Handedness"
    end
    enumeration "Handedness" do
      property "value", :type => "UML::String"
      literal "Ambidextrous"
      literal "Left Handed"
      literal "Right Handed"
    end
    package "Clown" do
      klass "Clown", :parents => ["People::Person"] do
        property "affiliations", :type => "People::Clown::ClownAffiliations", :upper => Float::INFINITY
      end
      klass "NestingDoll" do
        property "traits", :type => "People::Clown::DollTraits", :upper => Float::INFINITY
      end
      enumeration "ClownAffiliations" do
        property "value", :type => "UML::String"
        literal "Bozos United"
        literal "National Clown Association"
        literal "World Clown Union"
      end
      enumeration "DollTraits" do
        property "value", :type => "UML::String"
        literal "Fat"
        literal "Furry"
        literal "Happy"
      end
    end
  end
end
