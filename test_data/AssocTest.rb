project "AssocTest" do
  package "Automotive" do
    # one-to-one
    association :properties => ["Automotive::RepairShop::chief_mechanic", "Automotive::Mechanic::chief_for"]
    # many-to-one && one-to-many with the "one" side being an interface
    association :properties => ["Automotive::RepairShop::location", "Geography::Location::repair_shops"]
    # many-to-many ordered on both ends
    association :properties => ["Automotive::Vehicle::maintained_by", "People::Person::maintains"]
    # one-to-one with composition
    association :properties => ["Automotive::Vehicle::vin", "Automotive::VIN::vehicle"]
    klass "Car", :parents => ["Automotive::Vehicle"]
    klass "Component" do
      property "ownership", :type => "People::Ownership"
    end
    klass "Mechanic", :parents => ["People::Person"] do
      property "chief_for", :type => "Automotive::RepairShop", :lower => 1
    end
    klass "RepairShop" do
      property "chief_mechanic", :type => "Automotive::Mechanic", :lower => 1
      property "location", :type => "Geography::Location"
    end
    klass "VIN" do
      property "vehicle", :type => "Automotive::Vehicle"
    end
    klass "Vehicle", :implements => ["Automotive::Warrantied"], :is_abstract => true do
      property "drivers", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY
      property "maintained_by", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY
      property "owners", :type => "People::Person", :lower => 1, :upper => Float::INFINITY
      property "vin", :type => "Automotive::VIN", :aggregation => :composite, :lower => 1
    end
  end
  package "Geography" do
    # one-to-many && many-to-one
    association :properties => ["Geography::Address::city", "Geography::City::addresses"]
    klass "Address" do
      property "city", :type => "Geography::City"
    end
    klass "City", :implements => ["Geography::Location"] do
      property "addresses", :type => "Geography::Address", :upper => Float::INFINITY
    end
    interface "Location" do
      property "repair_shops", :type => "Automotive::RepairShop", :upper => Float::INFINITY
    end
  end
  package "People" do
    # many-to-many with association class
    association :properties => ["People::Person::drives", "Automotive::Vehicle::drivers"], :association_class => "People::Driving"
    # many-to-many with association class, ordered, ostensibly mandatory that a car must have an owner
    association :properties => ["Automotive::Vehicle::owners", "People::Person::vehicles"], :association_class => "People::Ownership"
    # association b/w association class and class in different package
    association :properties => ["People::Ownership::purchased_components", "Automotive::Component::ownership"]
    klass "Driving"
    klass "Ownership" do
      property "purchased_components", :type => "Automotive::Component", :upper => Float::INFINITY
    end
    klass "Person" do
      property "drives", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY
      property "maintains", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY
      property "vehicles", :type => "Automotive::Vehicle", :upper => Float::INFINITY
    end
    package "Clown" do
      # many-to-one / one-to-many self-association. Note that 
      association :properties => ["People::Clown::NestingDoll::inner_doll", "People::Clown::NestingDoll::outer_doll"]
      # one-to-many / many-to-one with mandatory "at least one", i.e. multiplicity of 1..*
      association :properties => ["People::Clown::Clown::unicycles", "People::Clown::Unicycle::clown"]
      klass "Clown" do
        property "unicycles", :type => "People::Clown::Unicycle", :lower => 1, :upper => Float::INFINITY
      end
      klass "NestingDoll" do
        property "inner_doll", :type => "People::Clown::NestingDoll", :aggregation => :composite
        property "outer_doll", :type => "People::Clown::NestingDoll"
      end
      klass "Unicycle", :parents => ["Automotive::Vehicle"] do
        property "clown", :type => "People::Clown::Clown"
      end
    end
  end
end
