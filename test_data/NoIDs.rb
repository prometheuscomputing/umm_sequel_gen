project "AssocTest" do
  package "Automotive" do
    association :properties => ["Automotive::RepairShop::chief_mechanic", "Automotive::Mechanic::chief_for"]
    association :properties => ["Automotive::Vehicle::components", "Automotive::Component::vehicle"]
    association :properties => ["Automotive::RepairShop::currently_working_on", "Automotive::Vehicle::being_repaired_by"]
    association :properties => ["Automotive::RepairShop::customers", "People::Person::repair_shops"]
    association :properties => ["Automotive::VehicleTaxRate::for_country", "Geography::Country::vehicle_tax_rate"]
    association :properties => ["Automotive::RepairShop::location", "Geography::Location::repair_shops"]
    association :properties => ["Automotive::Vehicle::maintained_by", "People::Person::maintains"]
    association :properties => ["Automotive::RepairShop::mechanics", "Automotive::Mechanic::employer"]
    association :properties => ["Automotive::Vehicle::occupants", "People::Person::occupying"]
    association :properties => ["Automotive::Component::parts", "Automotive::Part::component"]
    association :properties => ["Automotive::Warranty::replacement", "Automotive::Warrantied::replacement_for"]
    association :properties => ["Automotive::Part::sub_parts", "Automotive::Part::part_of"]
    association :properties => ["Automotive::Vehicle::vin", "Automotive::VIN::vehicle"]
    association :properties => ["Automotive::Warrantied::warranty", "Automotive::Warranty::warrantieds"]
    klass "Car", :parents => ["Automotive::Vehicle"] do
      property "inspection_completed", :type => "UML::Boolean"
      property "miles_per_gallon", :type => "UML::Integer"
      property "state", :type => "UML::String"
    end
    klass "Component", :implements => ["Automotive::Warrantied"] do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "name", :type => "UML::String"
      property "ownership", :type => "People::Ownership"
      property "parts", :type => "Automotive::Part", :aggregation => :composite, :upper => Float::INFINITY
      property "vehicle", :type => "Automotive::Vehicle"
    end
    klass "ElectricVehicle", :parents => ["Automotive::Vehicle"] do
      property "electric_efficiency", :type => "UML::Float"
      property "sponsor", :type => "Automotive::Sponsor"
    end
    klass "HybridVehicle", :parents => ["Automotive::Car", "Automotive::ElectricVehicle"] do
      property "hybrid_type", :type => "UML::String"
    end
    klass "Mechanic", :parents => ["People::Person"] do
      property "chief_for", :type => "Automotive::RepairShop", :lower => 1
      property "employer", :type => "Automotive::RepairShop", :lower => 1
      property "salary", :type => "Automotive::Salary"
    end
    klass "Minivan", :parents => ["Automotive::Car"] do
      property "sliding_doors", :type => "UML::Boolean"
    end
    klass "Motorcycle", :parents => ["Automotive::Vehicle"]
    klass "Part", :implements => ["Automotive::HasSerial"] do
      property "component", :type => "Automotive::Component"
      property "name", :type => "UML::String", :default_value => "n/a"
      property "part_of", :type => "Automotive::Part"
      property "sub_parts", :type => "Automotive::Part", :aggregation => :composite, :upper => Float::INFINITY
    end
    klass "RepairShop" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "chief_mechanic", :type => "Automotive::Mechanic", :lower => 1
      property "currently_working_on", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY
      property "customers", :type => "People::Person", :upper => Float::INFINITY
      property "location", :type => "Geography::Location"
      property "mechanics", :type => "Automotive::Mechanic", :upper => 5
      property "name", :type => "UML::String"
    end
    klass "Repair_Workflow" do
      property "project_name", :type => "UML::String"
    end
    klass "VIN" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "issue_date", :type => "UML::Date"
      property "vehicle", :type => "Automotive::Vehicle"
      property "vin", :type => "UML::Integer"
    end
    klass "Vehicle", :implements => ["Automotive::Warrantied"], :is_abstract => true do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "being_repaired_by", :type => "Automotive::RepairShop"
      property "components", :type => "Automotive::Component", :aggregation => :composite, :upper => Float::INFINITY
      property "cost", :type => "Automotive::Dollar"
      property "drivers", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY
      property "maintained_by", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY
      property "make", :type => "Automotive::VehicleMaker", :default_value => "Unknown"
      property "occupants", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY
      property "owners", :type => "People::Person", :lower => 1, :upper => Float::INFINITY
      property "registered_at", :type => "Geography::Address"
      property "vehicle_model", :type => "UML::String"
      property "vin", :type => "Automotive::VIN", :aggregation => :composite, :lower => 1
    end
    klass "VehicleTaxRate" do
      property "for_country", :type => "Geography::Country", :lower => 1
      property "property_tax_rate", :type => "UML::Float", :default_value => 5.1
      property "sales_tax_rate", :type => "UML::Float", :default_value => 10.5
    end
    klass "Warranty" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "coverage", :type => "UML::String"
      property "replacement", :type => "Automotive::Warrantied", :aggregation => :composite
      property "warrantieds", :type => "Automotive::Warrantied", :upper => Float::INFINITY
    end
    enumeration "DomesticVehicleMaker", :parents => ["Automotive::VehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Dodge"
      literal "Ford"
    end
    enumeration "ForeignVehicleMaker", :parents => ["Automotive::VehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Honda"
      literal "Volvo"
    end
    enumeration "OtherVehicleMaker", :parents => ["Automotive::DomesticVehicleMaker", "Automotive::ForeignVehicleMaker"] do
      property "value", :type => "UML::String"
      literal "Home Made"
      literal "Other"
      literal "Unknown"
    end
    enumeration "PaymentFrequency" do
      property "value", :type => "UML::String"
      literal "bi-weekly"
      literal "monthly"
      literal "none"
    end
    enumeration "Sponsor" do
      property "value", :type => "UML::String"
      literal "Other"
      literal "US Federal Governemnt"
    end
    enumeration "VehicleMaker" do
      property "value", :type => "UML::String"
    end
    interface "HasSerial" do
      property "serial", :type => "UML::Integer"
    end
    interface "HasWarranty", :is_abstract => true do
      property "warranty_expiration_date", :type => "UML::Date"
    end
    interface "Warrantied", :parents => ["Automotive::HasSerial", "Automotive::HasWarranty"] do
      property "replacement_for", :type => "Automotive::Warranty"
      property "warranty", :type => "Automotive::Warranty"
      property "warranty_void", :type => "UML::Boolean"
    end
    primitive "Dollar", :parents => ["UML::Integer"]
    primitive "Salary", :parents => ["Automotive::Dollar"]
  end
  package "Geography" do
    association :properties => ["Geography::State::cities", "Geography::City::state"]
    association :properties => ["Geography::Territory::cities", "Geography::City::territory"]
    association :properties => ["Geography::Address::city", "Geography::City::addresses"]
    association :properties => ["Geography::Address::registered_vehicles", "Automotive::Vehicle::registered_at"]
    association :properties => ["Geography::Country::states", "Geography::State::country"]
    association :properties => ["Geography::Country::territories", "Geography::Territory::country"]
    klass "Address" do
      property "city", :type => "Geography::City"
      property "person", :type => "People::Person"
      property "registered_vehicles", :type => "Automotive::Vehicle", :upper => Float::INFINITY
      property "street_name", :type => "UML::String"
      property "street_number", :type => "UML::Integer"
    end
    klass "City", :implements => ["Geography::Location"] do
      property "addresses", :type => "Geography::Address", :upper => Float::INFINITY
      property "state", :type => "Geography::State"
      property "territory", :type => "Geography::Territory"
    end
    klass "Country", :implements => ["Geography::Location"] do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "states", :type => "Geography::State", :aggregation => :composite, :upper => Float::INFINITY
      property "territories", :type => "Geography::Territory", :aggregation => :composite, :upper => Float::INFINITY
      property "vehicle_tax_rate", :type => "Automotive::VehicleTaxRate"
    end
    klass "State" do
      property "cities", :type => "Geography::City", :aggregation => :composite, :upper => Float::INFINITY
      property "country", :type => "Geography::Country"
      property "name", :type => "UML::String"
    end
    klass "Territory" do
      property "cities", :type => "Geography::City", :aggregation => :composite, :upper => Float::INFINITY
      property "country", :type => "Geography::Country"
      property "name", :type => "UML::String"
    end
    interface "Location" do
      property "name", :type => "UML::String"
      property "repair_shops", :type => "Automotive::RepairShop", :upper => Float::INFINITY
    end
  end
  package "People" do
    association :properties => ["People::Person::addresses", "Geography::Address::person"]
    association :properties => ["People::Person::aliases", "People::Alias::person"]
    association :properties => ["People::Person::drives", "Automotive::Vehicle::drivers"], :association_class => "People::Driving"
    association :properties => ["Automotive::Vehicle::owners", "People::Person::vehicles"], :association_class => "People::Ownership"
    association :properties => ["People::Ownership::purchased_components", "Automotive::Component::ownership"]
    klass "Driving" do
      property "car_review", :type => "Gui_Builder_Profile::RichText"
      property "likes_driving", :type => "UML::Boolean", :default_value => true
    end
    klass "Ownership" do
      property "payment_frequency", :type => "Automotive::PaymentFrequency"
      property "percent_ownership", :type => "UML::Float", :default_value => 100.0
      property "purchased_components", :type => "Automotive::Component", :upper => Float::INFINITY
    end
    klass "Person" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property "addresses", :type => "Geography::Address", :aggregation => :composite, :upper => Float::INFINITY
      property "aliases", :type => "People::Alias", :upper => Float::INFINITY
      property "date_of_birth", :type => "Gui_Builder_Profile::Date"
      property "dependent", :type => "UML::Boolean"
      property "description", :type => "Gui_Builder_Profile::RichText"
      property "drives", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY
      property "family_size", :type => "UML::Integer", :default_value => 1
      property "handedness", :type => "People::Handedness"
      property "last_updated", :type => "Gui_Builder_Profile::Timestamp"
      property "lucky_numbers", :type => "UML::Integer", :upper => Float::INFINITY
      property "maintains", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY
      property "manifesto", :type => "Gui_Builder_Profile::BigString"
      property "name", :type => "People::PersonName"
      property "occupying", :type => "Automotive::Vehicle"
      property "photo", :type => "Gui_Builder_Profile::File"
      property "repair_shops", :type => "Automotive::RepairShop", :upper => Float::INFINITY
      property "vehicles", :type => "Automotive::Vehicle", :upper => Float::INFINITY
      property "wakes_at", :type => "Gui_Builder_Profile::Time"
      property "weight", :type => "UML::Real"
    end
    enumeration "Handedness" do
      property "value", :type => "UML::String"
      literal "Ambidextrous"
      literal "Left Handed"
      literal "Right Handed"
    end
    package "Clown" do
      association :properties => ["People::Clown::Clown::dolls", "People::Clown::NestingDoll::clown"]
      association :properties => ["People::Clown::NestingDoll::inner_doll", "People::Clown::NestingDoll::outer_doll"]
      association :properties => ["People::Clown::Clown::unicycles", "People::Clown::Unicycle::clown"]
      klass "Clown", :parents => ["People::Person"] do
        property "affiliations", :type => "People::Clown::ClownAffiliations", :upper => Float::INFINITY
        property "clown_name", :type => "UML::String"
        property "dolls", :type => "People::Clown::NestingDoll", :upper => Float::INFINITY
        property "unicycles", :type => "People::Clown::Unicycle", :lower => 1, :upper => Float::INFINITY
      end
      klass "NestingDoll" do
        property "clown", :type => "People::Clown::Clown"
        property "color", :type => "UML::String"
        property "inner_doll", :type => "People::Clown::NestingDoll", :aggregation => :composite
        property "outer_doll", :type => "People::Clown::NestingDoll"
        property "traits", :type => "People::Clown::DollTraits", :upper => Float::INFINITY
      end
      klass "Unicycle", :parents => ["Automotive::Vehicle"] do
        property "clown", :type => "People::Clown::Clown"
        property "color", :type => "UML::String"
      end
      enumeration "ClownAffiliations" do
        property "value", :type => "UML::String"
        literal "Bozos United"
        literal "National Clown Association"
        literal "World Clown Union"
      end
      enumeration "DollTraits" do
        property "value", :type => "UML::String"
        literal "Fat"
        literal "Furry"
        literal "Happy"
      end
    end
    primitive "Alias", :parents => ["People::PersonName"] do
      property "person", :type => "People::Person"
    end
    primitive "PersonName", :parents => ["UML::String"]
  end
  profile "Gui_Builder_Profile" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::options"
    association :properties => ["Gui_Builder_Profile::InvitationCode::additional_email_requirements", "Gui_Builder_Profile::StringCondition::additional_email_requirement_for"]
    association :properties => ["Gui_Builder_Profile::ProjectOptions::email_requirements", "Gui_Builder_Profile::StringCondition::email_requirement_for"]
    association :properties => ["Gui_Builder_Profile::BinaryData::file", "Gui_Builder_Profile::File::binary_data"]
    association :properties => ["Gui_Builder_Profile::RichText::images", "Gui_Builder_Profile::RichTextImage::rich_text"]
    association :properties => ["Gui_Builder_Profile::Organization::invitation_codes", "Gui_Builder_Profile::InvitationCode::organizations"]
    association :properties => ["Gui_Builder_Profile::Perspective::invitation_codes", "Gui_Builder_Profile::InvitationCode::perspectives"]
    association :properties => ["Gui_Builder_Profile::ProjectOptions::password_requirements", "Gui_Builder_Profile::StringCondition::password_requirement_for"]
    association :properties => ["Gui_Builder_Profile::Organization::people", "Gui_Builder_Profile::Person::organizations"]
    association :properties => ["Gui_Builder_Profile::User::perspectives", "Gui_Builder_Profile::Perspective::users"]
    association :properties => ["Gui_Builder_Profile::User::roles", "Gui_Builder_Profile::UserRole::users"], :association_class => "Gui_Builder_Profile::RolePermissions"
    association :properties => ["Gui_Builder_Profile::InvitationCode::roles_granted", "Gui_Builder_Profile::UserRole::invitations"], :association_class => "Gui_Builder_Profile::GrantedPermissions"
    association :properties => ["Gui_Builder_Profile::Perspective::substitutions", "Gui_Builder_Profile::MultivocabularySubstitution::perspective"]
    klass "BinaryData" do
      property "data", :type => "UML::ByteString"
      property "file", :type => "Gui_Builder_Profile::File"
    end
    klass "Code" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "language", :type => "Gui_Builder_Profile::LanguageType"
    end
    klass "File" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "binary_data", :type => "Gui_Builder_Profile::BinaryData", :aggregation => :composite
      property "filename", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "mime_type", :type => "UML::String"
    end
    klass "GrantedPermissions" do
      property "role_manager", :type => "UML::Boolean"
    end
    klass "InvitationCode" do
      property "additional_email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY
      property "code", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "expires", :type => "Gui_Builder_Profile::Timestamp"
      property "organizations", :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY
      property "perspectives", :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY
      property "roles_granted", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY
      property "uses_remaining", :type => "UML::Integer"
    end
    klass "MultivocabularySubstitution" do
      property "master_word", :type => "UML::String"
      property "perspective", :type => "Gui_Builder_Profile::Perspective", :lower => 1
      property "replacement_word", :type => "UML::String"
    end
    klass "Organization" do
      property "description", :type => "Gui_Builder_Profile::RichText"
      property "invitation_codes", :type => "Gui_Builder_Profile::InvitationCode", :aggregation => :shared, :upper => Float::INFINITY
      property "name", :type => "UML::String"
      property "org_user_limit", :type => "UML::Integer", :default_value => 2
      property "people", :type => "Gui_Builder_Profile::Person", :aggregation => :shared, :upper => Float::INFINITY
    end
    klass "Person" do
      property "email", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "email_verified", :type => "UML::Boolean"
      property "first_name", :type => "UML::String"
      property "last_name", :type => "UML::String"
      property "organizations", :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY
    end
    klass "Perspective" do
      property "invitation_codes", :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY
      property "name", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "substitutions", :type => "Gui_Builder_Profile::MultivocabularySubstitution", :aggregation => :composite, :upper => Float::INFINITY
      property "users", :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY
    end
    klass "ProjectOptions" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::singleton"
      property "email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY
      property "password_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY
      property "user_registration", :type => "Gui_Builder_Profile::UserRegistrationType", :default_value => "Open"
    end
    klass "RegularExpressionCondition", :parents => ["Gui_Builder_Profile::StringCondition"] do
      property "regular_expression", :type => "Gui_Builder_Profile::RegularExpression" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
    end
    klass "RichText" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "images", :type => "Gui_Builder_Profile::RichTextImage", :aggregation => :composite, :upper => Float::INFINITY
      property "markup_language", :type => "Gui_Builder_Profile::MarkupType"
    end
    klass "RichTextImage" do
      property "image", :type => "Gui_Builder_Profile::File"
      property "rich_text", :type => "Gui_Builder_Profile::RichText"
    end
    klass "RolePermissions" do
      property "role_manager", :type => "UML::Boolean"
    end
    klass "StringCondition", :is_abstract => true do
      property "additional_email_requirement_for", :type => "Gui_Builder_Profile::InvitationCode"
      property "description", :type => "UML::String"
      property "email_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions"
      property "failure_message", :type => "UML::String"
      property "password_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions"
    end
    klass "User", :parents => ["Gui_Builder_Profile::Person"] do
      property "email_confirmation_token", :type => "UML::String"
      property "login", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "password_hash", :type => "UML::String"
      property "password_reset_time_limit", :type => "Gui_Builder_Profile::Timestamp"
      property "password_reset_token", :type => "UML::String"
      property "perspectives", :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY
      property "roles", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY
      property "salt", :type => "UML::String"
      property "use_accessibility", :type => "UML::Boolean"
    end
    klass "UserRole" do
      property "invitations", :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY
      property "name", :type => "UML::String" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "registration", :type => "Gui_Builder_Profile::RoleRegistrationType", :default_value => "Role Manager"
      property "users", :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY
    end
    enumeration "FacadeImplementationLanguages" do
      property "value", :type => "UML::String"
      literal "Java"
      literal "Ruby"
    end
    enumeration "LanguageType" do
      property "value", :type => "UML::String"
      literal "Clojure"
      literal "Groovy"
      literal "Javascript"
      literal "Python"
      literal "Ruby"
    end
    enumeration "MarkupType" do
      property "value", :type => "UML::String"
      literal "HTML"
      literal "Kramdown"
      literal "LaTeX"
      literal "Markdown"
      literal "Plain"
      literal "Textile"
    end
    enumeration "RoleRegistrationType" do
      property "value", :type => "UML::String"
      literal "Administrator"
      literal "Open"
      literal "Role"
      literal "Role Manager"
    end
    enumeration "UserRegistrationType" do
      property "value", :type => "UML::String"
      literal "Email"
      literal "Invitation"
      literal "Open"
    end
    package "OperationTypes" do
      stereotype "create", :metaclasses => ["Element"]
      stereotype "delete", :metaclasses => ["Element"]
      stereotype "other", :metaclasses => ["Element"]
      stereotype "retrieve", :metaclasses => ["Element"]
      stereotype "update", :metaclasses => ["Element"]
    end
    package "Spec" do
      klass "SpecButton" do
        property "action", :type => "UML::String"
        property "button_text", :type => "UML::String"
        property "display_result", :type => "Gui_Builder_Profile::Spec::SpecButtonResultType", :default_value => "none"
        property "image_actions", :type => "UML::String", :upper => Float::INFINITY
        property "label", :type => "UML::String"
        property "save_page", :type => "UML::Boolean", :default_value => true
      end
      klass "SpecLabel" do
        property "label_content", :type => "UML::String"
      end
      enumeration "SpecButtonResultType" do
        property "value", :type => "UML::String"
        literal "file"
        literal "none"
        literal "popup"
        literal "web_page"
      end
      enumeration "SpecSearchFilterType" do
        property "value", :type => "UML::String"
        literal "boolean"
        literal "case_insensitive_exact"
        literal "case_insensitive_like"
        literal "case_sensitive_exact"
        literal "chemical_system"
        literal "default"
        literal "disabled"
      end
      stereotype "disabled", :metaclasses => ["Property"]
      stereotype "expanded", :metaclasses => ["Property"]
      stereotype "hidden", :metaclasses => ["Property"]
      stereotype "label", :metaclasses => ["Element"] do
        tag "label_name"
      end
      stereotype "search_filter", :metaclasses => ["Property"] do
        tag "search_filter"
      end
      stereotype "show_possible", :metaclasses => ["Property"]
    end
    primitive "BigString", :parents => ["UML::String"]
    primitive "Date"
    primitive "RegularExpression", :parents => ["UML::String"]
    primitive "Time"
    primitive "Timestamp"
    stereotype "acts_as_user", :metaclasses => ["Class", "Interface"]
    stereotype "alt_root", :metaclasses => ["Class"]
    stereotype "change_tracked", :metaclasses => ["Package"]
    stereotype "complex_attribute", :metaclasses => ["Class"]
    stereotype "descriptor", :metaclasses => ["Property"]
    stereotype "facade", :metaclasses => ["Interface"]
    stereotype "facade_method", :metaclasses => ["Package"] do
      tag "implementation"
      tag "language"
    end
    stereotype "nil_for_root", :metaclasses => ["Property"]
    stereotype "options", :metaclasses => ["Package"] do
      tag "require"
      tag "version"
      tag "project_name"
      tag "plugins"
      tag "application_options"
    end
    stereotype "root", :metaclasses => ["Class"] do
      tag "Root Name"
    end
    stereotype "singleton", :metaclasses => ["Class"]
  end
end
