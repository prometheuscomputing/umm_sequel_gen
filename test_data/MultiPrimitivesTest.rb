project "MultiPrimitivesTest" do
  package "People" do
    association :properties => ["People::Person::aliases", "People::Alias::person"]
    association :properties => ["People::Person::nickname", "People::Alias::nickname_for"]
    klass "Person" do
      property "nickname", :type => "People::Alias"
      property "aliases", :type => "People::Alias", :upper => Float::INFINITY
      property "lucky_numbers", :type => "UML::Integer", :upper => Float::INFINITY
    end
    primitive "Alias", :parents => ["People::PersonName"] do
      property "person", :type => "People::Person"
      property "nickname_for", :type => "People::Person"
    end
    primitive "PersonName", :parents => ["UML::String"]
  end
end
